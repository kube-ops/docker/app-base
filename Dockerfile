ARG BUILD_DATE
ARG VCS_REF

FROM alpine:3.12 AS builder

RUN apk add --no-cache ca-certificates tzdata
RUN mkdir -p /app/data && chown -R 1042:1042 /app/data
RUN mkdir -p /app/bin /app/lib /app/configs /app/secrets

FROM scratch AS rootfs

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo/UTC /etc/localtime
COPY --from=builder /app /app
COPY --from=builder /tmp /tmp

FROM scratch

ARG BUILD_DATE
ARG VCS_REF

COPY --from=rootfs / /

LABEL \
    org.label-schema.build-date="${BUILD_DATE}" \
    org.label-schema.name="app-base" \
    org.label-schema.description="Base image for creating kubernetes applications" \
    org.label-schema.url="https://gitlab.com/kube-ops/docker/app-base" \
    org.label-schema.vcs-ref="${VCS_REF}" \
    org.label-schema.version="1.0.0" \
    org.label-schema.vcs-url="https://gitlab.com/kube-ops/docker/app-base" \
    org.label-schema.vendor="Anton Kulikov" \
    org.label-schema.schema-version="1.0"

ENV \
    CHARSET="UTF-8" \
    LANG="C.UTF-8" \
    TZ="UTC" \
    UID="1042" \
    GID="1042" \
    PATH="/app/bin" \
    LD_LIBRARY_PATH="/app/lib" \
    HOME="/app/data" \
    TMPDIR="/dev/shm"

WORKDIR /app
VOLUME [ "/app/data", "/app/configs", "/app/secrets" ]
